#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

unsigned long long low_, high_, timestamp;


#define TIME_STAMP                      \
    __asm__ __volatile__(               \
            "mfence;"                   \
            "rdtscp;"                   \
            : "=a" (low_), "=d" (high_)); \
    timestamp = low_ | (high_ << 32);



#define SIZE1  32/*** edit ***/
#define SIZE2 2
#define VICTIM_NUM 1 /*** edit ***/




int prime_array[SIZE1];
int probe_array[SIZE2];
int victim_probe[VICTIM_NUM] = {0}; /*** edit ***/

int prime_num = 3;
int probe_num = 1;

int prime_input = 0b10011001100110011001100110001001;//rand() % limit;//615; /*** edit ***/
int probe_input = 0b00;//prime_input;

void initialize(){
    srand(time(NULL));
}

unsigned long long* probe_store;
unsigned long long* prime_store;

unsigned long long limit;

void time_check(unsigned long long array[], int probe[], int size, int index);
unsigned long long time_stamp();
int Min(unsigned long long array[], int size);

#define A 20
#define B A*30
#define G 10
void nop_(){
}
static unsigned long long store[B];
void attack(int array[], int index, int size){
    int i, j;
    unsigned long long start, end;
    unsigned long long low, high;
    for(i = index; i < index + size; i++){
//        start = time_stamp();
//        start = TIME_STAMP;
//        start = TIME_STAMP;
        TIME_STAMP;
        start = timestamp;
        if(array[i])
            asm ("nop; nop;");
        TIME_STAMP;
        end = timestamp;
        store [i] = end - start;

        nop_();

    }
}
void vict(int array[], int index, int size){
    int i, j;
    unsigned long long start, end;
    for(i = index; i < index + size; i++){
        TIME_STAMP;
        start = timestamp;
        if(array[i])
            asm ("nop; nop;");
        TIME_STAMP;
        end = timestamp;
        store [i] = end - start;
    }
}

void __test(int array[], int index, int size){
    attack(array, index, size);
//            nop_();
}
void test(){

    int size = A;
    int total = B;
//    int temp[A] = {1, 0, 0, 0, 1, 1, 1, 0, 1, 0};
    int temp[A] = {0, 0, 1, 0, 0, 0, 1, 0, 0, 0};
    int temp_[A] = {0, 0, 1, 0, 0, 0, 1, 0, 0, 0};
    int temp__[G] = {1, 0, 1, 1, 0, 1, 1, 0, 1, 0};
    int array[B];
    
    int i,j;
    srand(time(NULL));
    for(i = 0; i < A; i++){
        temp[i] = rand() % 2;
    }

    for(i = 0; i < total; i++){
        array[i] = temp[i % A];
    }

/*
    for(i = total - size; i < total; i++){
        array[i] = temp_[i % A];
    }
    */
    unsigned long long start, end;
    for(j = 0; j < total/size; j++){
        for(i = 0; i < size; i++){
            printf("%d,\t", array[j * size + i]);
        }
        printf("\n");
    }

//    for(i = total - size; i < total; i++){
//        array[i] = temp_[i % A];
//    }


//    attack(array, total - A, A);
    for(i = 0; i < total/size ; i++){
        __test(array, i * size, size);
    }

   

//        __test(array, 19 * size, size);


    int min = Min(store, total);
    printf("min : %d\n", min);
    for(j = 0; j < total/size; j++){
        for(i = 0; i < size; i++){
            (store[j * size + i] > min + 28) ? printf("%c[1;31m",27) : printf("%c[1;32m",27);
          
//            char *result = (store[j * SIZE1 + i] > min + 32) ? "Miss" : "Hit";
            //            printf("%s,\t", result);

            printf("%llu|,\t", store[j * size + i]);
            printf("%c[0m",27); 
        }
        printf("\n");
    }
    printf("end\n");

}

unsigned long long time_stamp(){
    unsigned long long high, low;
    asm volatile(
            "mfence;"
            "rdtscp;"
            : "=a"(low), "=d"(high)
            );
    return (low)|((high) << 32);

}
void victim(){
    int i;
   for(i = 0; i < VICTIM_NUM; i++){
        if(victim_probe[i])
            asm("nop; nop; nop;");
    }
}
/*
void probe(int input, int count, int test){
    int i;
    for(i = 0; i < size; i++)
        time_check(probe_store, probe_array, SIZE, i);
}
*/

void pad(){
    //nop - self program
}
void Prime_Probe(int input, int count, int test, unsigned long long store[]){
    int i, temp;
    temp = input;
    int mask = 1U << (count-1);
    for(i = 0; i < count; i++){
        probe_array[i] = (input & mask) ? 1 : 0;
        input <<= 1;
    }
    for(i = 0; i < test; i++)
        time_check(store, probe_array, count, i);
}
void time_check(unsigned long long store[], int array[], int size, int index){
    unsigned long long start, end;
    int i;
    for(i = 0; i < size; i++){
        TIME_STAMP;
        start = timestamp;
        if(array[i])
            asm("nop; nop;");
        TIME_STAMP;
        end = timestamp;
        store[index * size + i] = end - start;
    }
}

int Min(unsigned long long array[], int size){
    int i, ret = 5000;
    for(i = 0; i < size; i++){
        if(ret > array[i] && array[i] != 0)
            ret = array[i];
    }
    return ret;
}
int Max(unsigned long long array[], int size){
    int i, ret = 0;
    for(i = 0; i < size; i++){
        if(ret < array[i])
            ret = array[i];
    }
    return ret;
}

int main(){
    initialize();
    limit = (unsigned long long)pow(2, SIZE1);
    printf("limit: %llu\n", limit);
    int i, j;

    test();
    printf("end\n");
/*
    probe_store = (unsigned long long *)malloc(sizeof(unsigned long long) * SIZE1 * probe_num);
    prime_store = (unsigned long long *)malloc(sizeof(unsigned long long) * SIZE2 * prime_num);

    Prime_Probe(prime_input, SIZE1, prime_num, prime_store);
//    for(i = 0; i < SIZE; i++)
//        printf("%d,\t", probe_array[i]);
//    printf("\n");
    victim();
    Prime_Probe(probe_input, SIZE2, probe_num, probe_store);
    for(i = 0; i < SIZE2; i++)
        printf("%d,\t", probe_array[i]);
    printf("\n");
    printf("******************* prime *******************\n");
    int min = Min(prime_store, SIZE1 * prime_num);
    printf("min: %d\n", min);
    for(j = 0; j < prime_num; j++){
        for(i = 0; i < SIZE1; i++){
            (prime_store[j * SIZE1 + i] > min + 32) ? printf("%c[1;31m",27) : printf("%c[1;32m",27);
            char *result = (prime_store[j * SIZE1 + i] > min + 32) ? "Miss" : "Hit";
//            printf("%s,\t", result);

                        printf("%llu|,\t", prime_store[j * SIZE1 + i]);
            printf("%c[0m",27); 
        }
        printf("\n");
    }
    printf("victim process...\n");
    printf("******************* probe *******************\n");
    min = Min(probe_store, SIZE2 * probe_num);
    printf("min: %d\n", min);
    for(j = 0; j < probe_num; j++){
        for(i = 0; i < SIZE2; i++){

            (probe_store[j * SIZE2 + i] > min + 32) ? printf("%c[1;31m",27) : printf("%c[1;32m",27);
            char *result = (probe_store[j * SIZE2 + i] > min + 32) ? "Miss" : "Hit";
//            printf("%s,\t", result);
                        printf("%llu|,\t", probe_store[j * SIZE2 + i]);
            printf("%c[0m",27); 
        }
        printf("\n");
    }
    printf("\n");
    */
}
