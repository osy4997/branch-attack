#include <stdio.h>

void sum_row1(double *a, double*b, long n){
    long i,j;
    for(i = 0; i < n; i++){
        b[i] = 0;
        for(j = 0; j < n; j++)
        {
            b[i] += a[i*n + j];
        }
    }
}

int main()
{
    double A[9] = {0, 1, 3, 5, 7, 11, 13, 17, 19};
    double *B = A + 6;
    int i; 
    sum_row1(A, B, 3);


}
