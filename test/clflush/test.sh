#!/bin/bash

gcc victim.c -o victim
gcc spy.c -o spy
taskset -c 0 ./victim & taskset -c 0 ./spy
