#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <pthread.h>
#include <sched.h>
unsigned long long low, high, timestamp;
#define SIZE 64
#define TIME_STAMP      \
    asm volatile(   \
            "mfence;"   \
            "rdtscp;"   \
            : "=a" (low), "=d" (high));\
    timestamp = low | (high << 32);

#define CLFLUSH(a) \
    asm volatile(   \
            "clflush (%0);" \
            :: "q"(a)   \
            );
//int probe[  ] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 
//int probe[  ] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8 ,9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 
int probe[SIZE] = {1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0};
//int probe[SIZE] = {1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0};
int pad_[32] = {0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1};

unsigned long long store[SIZE] = {0};
int miss[SIZE] = {0};

void pad(){
    asm(
            "nop;"
            "nop;"
//            "nop; nop; nop; nop; nop; nop; nop;"
//            "nop;"
       );
}

unsigned long long abs_(long long a){
    unsigned long long const mask = a >> sizeof(unsigned long long) * CHAR_BIT - 1;
    return (a + mask) ^ mask;
//    return a * ((a > 0) - (a < 0));
}
#define Min(a,b) ((a+b)-abs_(a-b)) / 2;
void flush(int *a){
    asm volatile(
            "clflush (%0);"
            :: "q"(a)
            );
}
void branch(int index){
    int i;
    unsigned long long start, end;
    for(i = 0; i < 128; i++){
//        if(pad_[i])
//            asm ("nop;");
    }
    for(i = 0; i < SIZE; i++){

//        CLFLUSH(&probe[i]);
//        int a = probe[i];
//        probe[i] = a;

        TIME_STAMP;
        start = timestamp;
        if(probe[i])
            asm("nop; nop;");
        TIME_STAMP;
        end = timestamp;
        store[i] = end-start;        
    }
    
}

int main(){
    int i, j, k;
    void* ptr = &branch;
//    printf("%p\n", ptr);
    for(k = 0; k < 5000; k++){
        for(i = 0; i < 1; i++){
            branch(i);
        }
        unsigned long long min = 5000;
        for(i = 0; i < SIZE; i++){
            long long a = (long long)min;
            long long b = (long long)store[i];
            min = Min(min, store[i]);
        }


        for(i = 0; i < SIZE; i++){
//            int mi = (abs_(store[i] - (min + 28)) / (store[i] - (min + 28)) + 1) / 2;
            int mi = (store[i] > min + 28);
            miss[i] += mi;
        }


//        unsigned long long min = MIN();

        for(i = 0; i < SIZE; i++){
//                (store[i] > min + 28) ? printf("%c[1;31m",27) : printf("%c[1;32m",27);
                printf("%llu,\t", store[i]);
//                printf("%c[0m",27);
            }
            printf("\n");

//        printf("victim\n");
        usleep(100);
//        sleep(0);
//        pthread_yield(;
//        if(sched_yield())
//            printf("no\n");
        

//            sleep(1);
    }

    for(i = 0; i < SIZE;i ++){
//        printf("victim - %d. %d\n", i, miss[i]);
    }

}
