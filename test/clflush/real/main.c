#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

int main()
{
    int i, j;
    for(j = 0; j < 5000; j++){
        for(i = 0; i < 50; i++)
        {
            if(fork() == 0){

                system("taskset -c 0 ./victim");
                return 0;
                exit(1);
            }
        }
        system("taskset -c 0 ./spy");
    }
    return 0;
}
