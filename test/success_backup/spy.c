#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <pthread.h>
#include <sched.h>
unsigned long long low, high, timestamp;

#define TIME_STAMP      \
    asm volatile(   \
            "mfence;"   \
            "rdtscp;"   \
            : "=a" (low), "=d" (high));\
    timestamp = low | (high << 32);

int probe[20] = {0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1};
unsigned long long store[20] = {0};
int miss[20] = {0};

unsigned long long abs_(unsigned long long a){
    unsigned long long const mask = a >> sizeof(unsigned long long) * CHAR_BIT - 1;
    return (a + mask) ^ mask;
}
#define Min(a,b) ((a+b)-abs_(a-b)) / 2;
unsigned long long time_stamp(){
    unsigned long long low, high;
    asm volatile(
            "mfence;"
            "rdtscp;"
            : "=a" (low), "=d" (high)
            );
    return low + (high << 32);

}
void branch(int index){
    int i;
    unsigned long long start, end;
    for(i = 0; i < 20; i++){

        TIME_STAMP;
        start = timestamp;
//        asm volatile("nop; nop; nop; nop; nop; nop; nop; nop; nop;  nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;"); 
            if(probe[i])
                asm("nop; nop;");
//asm volatile("nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;"); 
        TIME_STAMP;
        end = timestamp;
        store[i] = end-start;        
    }
    
}

int main(){
    int i, j, k;
    void* ptr = &branch;
    printf("%p\n", ptr);
    for(k = 0; k < 10000; k++){
        for(i = 0; i < 2; i++){
            branch(i);
        }
        unsigned long long min = 5000;
        for(i = 0; i < 20; i++){
            min = Min(min, store[i]);
        }


        for(i = 0; i < 20; i++){
//            int mi = (abs_(store[i] - (min + 28)) / (store[i] - (min + 28)) + 1) / 2;
            int mi = (store[i] > min + 28);
            miss[i] += mi;
        }


//        unsigned long long min = MIN();

//        for(i = 0; i < 20; i++){
//                (store[i] > min + 28) ? printf("%c[1;31m",27) : printf("%c[1;32m",27);
//                printf("%llu,\t", store[i]);
//                printf("%c[0m",27);
//            }
//            printf("\n");

        usleep(400);
//        pthread_yield();
//        if(sched_yield())
//            printf("no\n");
        

//            sleep(1);
    }

    for(i = 0; i < 20;i ++){
        printf("spy - %d. %d\n", i, miss[i]);
    }

}
