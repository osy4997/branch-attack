#include <stdio.h>
#include <limits.h>
unsigned long long abs_( unsigned long long a){
    unsigned long long ret;
    unsigned long long const mask = a >> sizeof(unsigned long long) * CHAR_BIT - 1;
    return (a + mask) ^ mask;
}
int main(){

    unsigned long long a = -1;
    printf("%llu\n", abs_(a));

}
