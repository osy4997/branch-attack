#include <stdio.h>
#include <math.h>


int main(int argc, char *argv[]){
    srand(time(NULL));
    int limit = atoi(argv[1]);
    int i;
    FILE* stream = fopen("rand.txt", "w"); 

    for(i = 0; i < limit; i++){
        fprintf(stream, "%d\n", rand() % 2);
    }
    fclose(stream);
    return ;
}
